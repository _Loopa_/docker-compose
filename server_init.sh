#!/usr/bin/env bash

apt update && apt install -y ntp mc htop screen wget docker.io docker docker-compose build-essential python-dev libreadline-dev libbz2-dev libssl-dev libsqlite3-dev libxslt1-dev libxml2-dev git curl mysql-client libmysqlclient-dev python3-dev
systemctl enable docker
service ntp restart
