.gitlab-ci.yml
```
# SSH_PRIVATE_KEY = "==...=="
# STAGE_HOST_master = 123.123.123.123
#
#   Структура выполнения развертывания (deployment):
#                                      названия стадий:
#
#           Stage 1              Stage 2           Stage 3
#     |        - job 1     |      - job 5      |    -job 8        |
#     |        - job 2     |      - job 6      |                  |
#     |        - job 3     |     - job 7       |                  |
#     |        - job 4     |                   |                  |
#     |        - job 5     |                   |                  |
#
#
#             по сути, stage - это набор заданий, которые выполняются в нем
#
stages: # описываются стадии, которые будет выполнять ci/cd при развертывании
  - Build
  - Stage
  - Deploy

# далее по очереди описываются задания (jobs)
build: # имя задания
  stage: Build # название стадии, к которой привязывается задание
  image: docker:1.11
  tags:
    - docker
  services:
    - docker:dind
  artifacts:
    paths:
      - deploy
  script:
    - docker version
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
    - rm -f .gitignore .gitlab-ci.yml Rakefile rsync-include.txt README.md
    - rm -rf ./.git
    - docker build -t $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA .
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA

    - docker tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME

    - docker tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA $CI_REGISTRY_IMAGE:latest
    - docker push $CI_REGISTRY_IMAGE:latest

stage:
  stage: Stage
  variables:
    GIT_STRATEGY: none
  script:
    - apt-get update
    - apt-get install -y -qq ssh rsync
    - mkdir "${HOME}/.ssh"
    - echo "${SSH_PRIVATE_KEY}" > "${HOME}/.ssh/id_rsa"
    - chmod 700 "${HOME}/.ssh/id_rsa"
    - export TARGET_HOST="STAGE_HOST_${CI_COMMIT_REF_NAME}"
    - rsync -zavP -e "ssh -o StrictHostKeyChecking=no" ./deploy ${!TARGET_HOST}:/root/
    - ssh -o "StrictHostKeyChecking=no" ${!TARGET_HOST} "cd /root/deploy && ./pull_restart.sh ${CI_PROJECT_NAMESPACE} ${CI_PROJECT_NAME} gitlab-ci-token $CI_BUILD_TOKEN"
```
Dockerfile
```
FROM scratch

COPY . /root
```
pull_gitlab.sh

```
#!/usr/bin/env bash
name="${1,,}"

docker login -u $2 -p $3 registry.gitlab.com

docker pull registry.gitlab.com/$name:latest || exit

docker save registry.gitlab.com/$name:latest > git_src.tar

docker rmi --force $(docker images -q registry.gitlab.com/$name | uniq)

tar -xf git_src.tar --wildcards "*.tar" --strip-components 1
#rm  git_src.tar

tar -xf layer.tar --strip-components 1
rm layer.tar
```
pull_restart.sh
```
#!/bin/bash -l
if [ -z "$1" ]; then
    echo 'arg <project name>'
    exit 1
fi

# set -a; . /root/.server_env; set +a
# env | grep SERVER

[[ -d $2 ]] || mkdir $2
cd "$2" || exit 1

docker-compose down
docker-compose kill
docker volume prune
# -f --filter "label!=keep"
#[[ $SERVER_ENVIRONMENT == 'PRODUCTION' ]] && docker-compose rm -f -v
#[[ $SERVER_ENVIRONMENT == 'PRODUCTION' ]] && rm -rf .

#docker-compose rm -f -v
rm -rf ../$2/* || exit 1

../pull_gitlab.sh $1/$2 $3 $4
#cp -f ../envs/$1.env .env
cp -f server.docker-compose.yaml docker-compose.yaml
./init.sh || exit 1
docker-compose up -d --build --force-recreate --remove-orphans
```